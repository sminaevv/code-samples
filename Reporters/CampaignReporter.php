<?php

namespace App\Reporters;

use App\Models\Campaign;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Clickadilla\Support\Money\Currencies;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use InvalidArgumentException;
use Money\Currency;
use Money\Money;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;

class CampaignReporter extends Reporter
{
    protected $timeframe = self::TIMEFRAME_DAY;

    const GROUP_BY_PERIOD = 'period';
    const GROUP_BY_CAMPAIGN = 'campaign';
    const GROUP_BY_HOUR = 'hour';
    const GROUP_BY_BANNER = 'banner';
    const GROUP_BY_IMAGE = 'image';

    /**
     * Значение net_price до этой даты не сохранялось, при подсчете профита игнорируем более ранние записи
     */
    const NET_PRICE_COLLECTED_SINCE = '2018-05-01 00:00:00';

    private $campaings = [];
    private $groupBy = self::GROUP_BY_PERIOD;
    private $statuses = [];
    private $adFormats = [];
    private $users = [];
    private $admins = [];
    private static $groupByMethods = [
        self::GROUP_BY_PERIOD => 'statsByPeriod',
        self::GROUP_BY_CAMPAIGN => 'statsByCampaign',
        self::GROUP_BY_HOUR => 'statsByHour',
        self::GROUP_BY_BANNER => 'statsByBanner',
        self::GROUP_BY_IMAGE => 'statsByImage',
    ];

    /**
     * @return \Money\Money
     */
    public function cost()
    {
        return $this->statisticBuilderSum('campaign_statistics.price');
    }

    /**
     * @return Money
     */
    public function netCost(): Money
    {
        return $this->statisticBuilderSum('campaign_statistics.net_price');
    }

    /**
     * @return Money
     */
    public function revenue(): Money
    {
        $resultSet = $this->statisticBuilder()
            ->where('campaign_statistics.date', '>=', self::NET_PRICE_COLLECTED_SINCE)
            ->select(DB::raw('SUM(campaign_statistics.price - campaign_statistics.net_price) as revenue'));

        return new Money($resultSet->value('revenue') ?? 0, new Currency(Currencies::CODE));
    }

    private function statisticBuilderSum(string $column): Money
    {
        $sum = $this->statisticBuilder()->sum($column);

        return new Money($sum, new Currency(Currencies::CODE));
    }

    public function campaignStatsByDate()
    {
        $stats = $this->statisticBuilder()
            ->select([
                DB::raw('DATE_FORMAT(date, "%Y-%m-%d") as date'),
                DB::raw('SUM(price) as sum_cost'),
                DB::raw('SUM(net_price) as sum_net_cost'),
                DB::raw('SUM(campaign_statistics.price - campaign_statistics.net_price) as revenue')
            ])
            ->where('campaign_statistics.date', '>=', self::NET_PRICE_COLLECTED_SINCE)
            ->when($this->admins(), function ($query, $admins) {
                $query->join('users', 'users.id', '=', 'campaigns.user_id')
                    ->whereIn('admin_id', $admins);
            })
            ->groupBy(DB::raw('DATE_FORMAT(campaign_statistics.date, "%Y-%m-%d")'))
            ->get()
            ->mapWithKeys(function ($value) {
                /** @var Money $cost */
                $cost = int_to_money($value->sum_cost);
                /** @var Money $netCost */
                $netCost = int_to_money($value->sum_net_cost);
                return [
                    $value->date => [
                        'cost' => $cost,
                        'net_cost' => $netCost,
                        'revenue' => int_to_money($value->revenue),
                        'profit_rate' => (string)round($cost->ratioOf($netCost), 2),
                    ]
                ];
            })
            ->all();

        $result = [];
        $zeroMoney = str_to_money(0);
        for ($since = $this->since()->copy(); $since->lessThanOrEqualTo($this->till()); $since->addDay()) {
            $date = $since->toDateString();
            if (empty($stats[$date])) {
                $result[$date] = [
                    'cost' => $zeroMoney,
                    'net_cost' => $zeroMoney,
                    'revenue' => $zeroMoney,
                    'profit_rate' => '0',
                ];
            } else {
                $result[$date] = $stats[$date];
            }
        }
        return $result;
    }

    public function campaigns($campaign_ids = null)
    {
        if (!func_num_args()) {
            return $this->campaings;
        }

        if (!empty($campaign_ids)) {
            $this->campaings = array_filter((array)$campaign_ids);
        }

        return $this;
    }

    public function users(array $userIds = null)
    {
        if (is_null($userIds)) {
            return $this->users;
        }

        $this->users = $userIds;

        return $this;
    }

    public function admins(array $adninIds = null)
    {
        if (is_null($adninIds)) {
            return $this->admins;
        }

        $this->admins = $adninIds;

        return $this;
    }

    // Periods

    public function currentMonth()
    {
        return $this
            ->since((new Carbon())->startOfMonth())
            ->till((new Carbon())->endOfMonth());
    }

    public function previousMonth()
    {
        return $this
            ->since((new Carbon())->subMonth()->startOfMonth())
            ->till((new Carbon())->subMonth()->endOfMonth());
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function stats()
    {
        $method = static::$groupByMethods[$this->groupBy()];
        $stats = $this->{$method}();

        return $stats->transform(function ($item) {
            if (optional($item)->cost !== null) {
                $item->cost = new Money($item->cost, new Currency(Currencies::CODE));
            }
            return $item;
        });
    }

    /**
     * Get statistic info for campaign(s) grouped by selected timeframe.
     *
     * @return \Illuminate\Support\Collection
     */
    protected function statsByPeriod()
    {
        if ($this->timeframe() === self::TIMEFRAME_HOUR) {
            $select_period_sql = DB::raw('DATE_FORMAT(campaign_statistics.date, "%Y-%m-%d %H:00") as period');
        } else {
            $select_period_sql = DB::raw('DATE(campaign_statistics.date) as period');
        }

        /** @var  \Illuminate\Support\Collection $stats */
        $stats = static::statisticBuilder()
            ->select(
                $select_period_sql,
                DB::raw('SUM(campaign_statistics.impression) as impressions'),
                DB::raw('SUM(campaign_statistics.price) as cost'),
                DB::raw('SUM(campaign_statistics.clicks) as clicks')
            )
            ->groupBy('period')
            ->get();

        return $this->populateNoDataPeriods($stats);
    }

    private function populateNoDataPeriods(Collection $stats)
    {
        if ($this->timeframe() === self::TIMEFRAME_HOUR) {
            $interval = CarbonInterval::hour();
            $format = 'Y-m-d H:00';
        } else {
            $interval = CarbonInterval::day();
            $format = 'Y-m-d';
        }

        $stats_by_period_key = $stats->keyBy('period');
        $stats_full = new Collection();
        $date = $this->since()->copy();

        do {
            $stat = $stats_by_period_key->get($date->format($format));
            if (!$stat) {
                $stats_full->push((object)[
                    'period' => $date->format($format),
                    'impressions' => 0,
                    'cost' => 0,
                    'clicks' => 0,
                ]);
            } else {
                $stats_full->push($stat);
            }

            $date->add($interval);
        } while ($this->till()->greaterThan($date) || $this->till()->isSameAs($format, $date));

        return $stats_full;
    }

    /**
     * Get statistic info for campaign(s) grouped by hours.
     *
     * @return \Illuminate\Support\Collection
     */
    protected function statsByHour()
    {
        $stats = static::statisticBuilder()
            ->select(
                DB::raw('DATE_FORMAT(campaign_statistics.date, "%H") as hour'),
                DB::raw('SUM(campaign_statistics.impression) as impressions'),
                DB::raw('SUM(campaign_statistics.price) as cost'),
                DB::raw('SUM(campaign_statistics.clicks) as clicks')
            )
            ->groupBy('hour')
            ->get();

        return $this->populateNoDataHours($stats);
    }

    private function populateNoDataHours(Collection $stats)
    {
        $stats = $stats->keyBy('hour');

        return (new Collection(range(0, 23)))->map(function ($item) use ($stats) {
            $hour = sprintf('%02d', $item);
            $hourStats = $stats->get($hour);

            return (object)[
                'hour' => $hour,
                'impressions' => $hourStats->impressions ?? 0,
                'cost' => $hourStats->cost ?? 0,
                'clicks' => $hourStats->clicks ?? 0
            ];
        });
    }

    protected function statsByImage()
    {
        return static::bannerStatisticBuilder()
            ->select(
                'image_id',
                'src',
                DB::raw('ANY_VALUE(banner_id) as banner_id'),
                DB::raw('SUM(impression) as impressions'),
                DB::raw('SUM(clicks) as clicks')
            )
            ->join('banner_images', 'banner_statistics.image_id', '=', 'banner_images.id', 'inner')
            ->groupBy('image_id')
            ->get();
    }

    protected function statsByBanner()
    {
        return static::bannerStatisticBuilder()
            ->select(
                'banners.id as id',
                'banners.name as banner',
                'type',
                DB::raw('SUM(impression) as impressions'),
                DB::raw('SUM(clicks) as clicks')
            )
            ->join('banners', 'banner_statistics.banner_id', '=', 'banners.id', 'inner')
            ->groupBy('banner_id')
            ->get();
    }

    /**
     * Joins campaigns|campaign_statistics and filter by user_id, date range and campaigns
     *
     * @return Builder
     */
    private function statisticBuilder(): Builder
    {
        /** @var Builder $builder */
        $builder = DB::table('campaigns')
            ->join('campaign_statistics', 'campaigns.id', '=', 'campaign_statistics.campaign_id', 'inner')
            ->whereBetween('campaign_statistics.date', $this->dateRange());

        $this->campaignsFiltering($builder);

        return $builder;
    }

    private function bannerStatisticBuilder()
    {
        /** @var Builder $builder */
        $builder = DB::table('campaigns')
            ->join('banner_statistics', 'campaigns.id', '=', 'banner_statistics.campaign_id', 'inner')
            ->whereBetween('banner_statistics.date', $this->dateRange());

        $this->campaignsFiltering($builder);

        return $builder;
    }

    private function campaignsFiltering(Builder $builder)
    {
        if ($this->users()) {
            $builder->whereIn('campaigns.user_id', $this->users());
        }

        if ($this->campaigns()) {
            $builder->whereIn('campaigns.id', $this->campaigns());
        }

        if ($this->adFormats()) {
            $builder->whereIn('ad_format', $this->adFormats());
        }

        if ($this->statuses()) {
            if (!in_array('archived', $this->statuses(), true)) {
                $builder->whereNull('campaigns.deleted_at');
            };
            $builder->whereIn('campaigns.status', $this->statuses());
        }
    }

    public function userCampaignsBuilder(): EloquentBuilder
    {
        /** @var EloquentBuilder $builder */
        $builder = Campaign::query();

        $builder->when($this->users(), function ($query, $users) {
            $query->whereIn('user_id', $users);
        });

        return $builder;
    }

    /**
     * Make base query object to get user's campaigns with applied parameters.
     *
     * @return EloquentBuilder
     */
    private function campaignsBuilder(): EloquentBuilder
    {
        /** @var EloquentBuilder $builder */
        $builder = $this->userCampaignsBuilder();

        if ($this->campaigns()) {
            $builder->whereIn('id', $this->campaigns());
        }

        if ($this->adFormats()) {
            $builder->whereIn('ad_format', $this->adFormats());
        }

        if ($this->statuses()) {
            if (in_array('archived', $this->statuses(), true)) {
                $builder->withTrashed();
            };
            $builder->whereIn('status', $this->statuses());
        }

        return $builder;
    }

    /**
     * Get statistic info for campaign(s) grouped by campaigns.
     *
     * @return Collection
     */
    protected function statsByCampaign(): Collection
    {
        /** @var \Illuminate\Database\Query\Builder $builder */
        $stats = static::statisticBuilder()
            ->select(
                'campaigns.id',
                DB::raw('SUM(campaign_statistics.impression) as impressions'),
                DB::raw('SUM(campaign_statistics.price) as cost'),
                DB::raw('SUM(campaign_statistics.clicks) as clicks')
            )
            ->groupBy('campaigns.id')
            ->get();

        return $this->populateNoDataCampaign($stats);
    }

    private function populateNoDataCampaign(Collection $stats): Collection
    {
        $campaigns = $this->campaignsBuilder()->get();
        $stats = $stats->keyBy('id');

        return $campaigns->map(function ($campaign) use ($stats) {
            $item = [
                'campaign_id' => $campaign->id,
                'campaign' => $campaign->name,
                'impressions' => 0,
                'cost' => 0,
                'clicks' => 0,
            ];

            tap($stats->get($campaign->id), function ($instance) use (&$item) {
                if (isset($instance)) {
                    $item['impressions'] = $instance->impressions;
                    $item['cost'] = $instance->cost;
                    $item['clicks'] = $instance->clicks;
                }
            });

            return (object)$item;
        });
    }

    public function statuses($statuses = null)
    {
        if (!func_num_args()) {
            return $this->statuses;
        }

        if (!empty($statuses)) {
            $this->statuses = array_filter((array)$statuses);
        }

        return $this;
    }

    public function adFormats($adFormats = null)
    {
        if (!func_num_args()) {
            return $this->adFormats;
        }

        if (!empty($adFormats)) {
            $this->adFormats = array_filter((array)$adFormats);
        }

        return $this;
    }

    public function groupBy($groupBy = null)
    {
        if (!func_num_args()) {
            return $this->groupBy;
        }

        if (!empty($groupBy)) {
            if (!array_key_exists($groupBy, static::$groupByMethods)) {
                throw new InvalidArgumentException('Invalid groupBy clause');
            }

            $this->groupBy = $groupBy;
        }

        return $this;
    }
}
