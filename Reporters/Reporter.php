<?php

namespace App\Reporters;

use App\Exceptions\DateRangeLimitException;
use Carbon\Carbon;
use ErrorException;
use InvalidArgumentException;

/**
 * @todo: move to trait in `clickadilla/components` and reuse e.g. in StatsProvider's `ReportParams` class
 */
abstract class Reporter
{
    const TIMEFRAME_BLANK = 'blank';
    const TIMEFRAME_DAY = 'day';
    const TIMEFRAME_HOUR = 'hour';

    const TIMEFRAMES = [
        self::TIMEFRAME_BLANK,
        self::TIMEFRAME_DAY,
        self::TIMEFRAME_HOUR,
    ];

    /**
     * Max size of date range in days to build report for.
     *
     * @var int
     */
    protected $dateRangeLimit;
    protected $since;
    protected $till;
    protected $timeframe;


    protected function validateDateRange()
    {
        if (isset($this->dateRangeLimit, $this->since, $this->till)) {
            if ($this->since()->diffInDays($this->till()) > $this->dateRangeLimit()) {
                throw new DateRangeLimitException(sprintf('Date range must be under %s days', $this->dateRangeLimit()));
            }
        }
    }

    public function dateRange(): array
    {
        $this->validateDateRange();

        return [$this->since(), $this->till()];
    }


    public function dateRangeLimit(int $limit = null)
    {
        if (is_null($limit)) {
            return $this->dateRangeLimit;
        }

        $this->dateRangeLimit = $limit;

        return $this;
    }

    public function since(Carbon $datetime = null)
    {
        if (is_null($datetime)) {
            if (!$this->since) {
                throw new ErrorException('Start time is not set');
            }

            if ($this->timeframe() === self::TIMEFRAME_HOUR) {
                return $this->since->copy()->minute(0)->second(0);
            } elseif ($this->timeframe() === self::TIMEFRAME_DAY) {
                return $this->since->copy()->startOfDay();
            } elseif ($this->timeframe() === self::TIMEFRAME_BLANK) {
                return $this->since->copy();
            }
        }

        $this->since = $datetime->copy();

        return $this;
    }

    public function till(Carbon $datetime = null)
    {
        if (is_null($datetime)) {
            if (!$this->till) {
                throw new ErrorException('End time is not set');
            }

            if ($this->timeframe() === self::TIMEFRAME_HOUR) {
                return $this->till->copy()->minute(59)->second(59);
            } elseif ($this->timeframe() === self::TIMEFRAME_DAY) {
                return $this->till->copy()->endOfDay();
            } elseif ($this->timeframe() === self::TIMEFRAME_BLANK) {
                return $this->till->copy();
            }
        }

        $this->till = $datetime->copy();

        return $this;
    }

    public function timeframe($timeframe = null)
    {
        if (is_null($timeframe)) {
            if (!$this->timeframe) {
                throw new ErrorException('Timeframe is not set');
            }

            return $this->timeframe;
        }

        if (!in_array($timeframe, self::TIMEFRAMES, true)) {
            throw new InvalidArgumentException('Invalid timeframe passed');
        }

        $this->timeframe = $timeframe;

        return $this;
    }

    public function today()
    {
        return $this
            ->since(new Carbon())
            ->till(new Carbon());
    }

    public function yesterday()
    {
        return $this
            ->since((new Carbon())->subDay())
            ->till((new Carbon())->subDay());
    }
}
