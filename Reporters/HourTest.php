<?php

namespace Tests\Unit\Helpers;

use App\Helpers\Hour;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Tests\TestCase;

class HourTest extends TestCase
{
    public function testDiffMultipleHours()
    {
        $hour1 = new Hour('2017-10-10 10:32:26');
        $hour2 = new Hour('2017-10-10 11:48:11');

        $this->assertEquals(2, $hour1->diff($hour2));
    }

    public function testDiffSameHour()
    {
        $hour = new Hour();

        $this->assertEquals(1, $hour->diff($hour));
    }

    public function testHourSetup()
    {
        $hour = new Hour('2017-10-10 10:10:10');

        $this->assertNotSame($hour->start, $hour->end);
        $this->assertEquals(new Carbon('2017-10-10 10:00:00'), $hour->start);
        $this->assertEquals(new Carbon('2017-10-10 11:00:00'), $hour->end);
    }

    public function testIsCurrentForCurrentHour()
    {
        $this->assertTrue((new Hour())->isCurrent());
    }

    public function testIsCurrentForFutureHour()
    {
        $futureHour = (new Carbon())->addHour()->minute(0)->second(0);

        $this->assertFalse((new Hour($futureHour))->isCurrent());
    }

    public function testIsCurrentForPastHour()
    {
        $pastHour = (new Carbon())->subHour()->minute(59)->second(59);

        $this->assertFalse((new Hour($pastHour))->isCurrent());
    }

    public function testIsPastForCurrentHour()
    {
        $this->assertTrue((new Hour())->isPast());
    }

    public function testIsPastForFutureHour()
    {
        $futureHour = (new Hour())->next();

        $this->assertFalse($futureHour->isPast());
    }

    public function testIsPastForPastHour()
    {
        $hour = (new Hour())->prev();

        $this->assertTrue($hour->isPast());
    }

    public function testLoopBreak()
    {
        $from = new Hour('2017-10-10 01:05:00');
        $to = new Hour('2017-10-10 03:25:00');

        $iteratedHours = new Collection();

        Hour::loop($from, $to, function ($hour) use ($iteratedHours) {
            if ((string)$hour === '2017-10-10 02:00:00') {
                return false;
            }

            $iteratedHours->push($hour);
        });

        $this->assertEquals(1, $iteratedHours->count());
    }

    public function testLoopThroughMultipleHours()
    {
        $from = new Hour('2017-10-10 01:05:00');
        $to = new Hour('2017-10-10 03:25:00');

        $iteratedHours = new Collection();

        Hour::loop($from, $to, function ($hour) use ($iteratedHours) {
            $iteratedHours->push($hour);
        });

        $this->assertEquals(3, $iteratedHours->count());
    }

    public function testLoopThroughMultipleHoursBackward()
    {
        $from = new Hour('2017-10-10 01:05:00');
        $to = new Hour('2017-10-10 03:25:00');

        $iteratedHours = new Collection();

        Hour::loop($to, $from, function ($hour) use ($iteratedHours) {
            $iteratedHours->push($hour);
        });

        $this->assertEmpty($iteratedHours->diffAssoc([
            '2017-10-10 03:00:00',
            '2017-10-10 02:00:00',
            '2017-10-10 01:00:00',
        ])->all());
    }

    public function testLoopThroughMultipleHoursForward()
    {
        $from = new Hour('2017-10-10 01:05:00');
        $to = new Hour('2017-10-10 03:25:00');

        $iteratedHours = new Collection();

        Hour::loop($from, $to, function ($hour) use ($iteratedHours) {
            $iteratedHours->push($hour);
        });

        $this->assertEmpty($iteratedHours->diffAssoc([
            '2017-10-10 01:00:00',
            '2017-10-10 02:00:00',
            '2017-10-10 03:00:00',
        ])->all());
    }

    public function testLoopThroughSingleHour()
    {
        $from = new Hour('2017-10-10 01:00:00');
        $to = new Hour('2017-10-10 01:00:00');

        $iteratedHours = new Collection();

        Hour::loop($from, $to, function ($hour) use ($iteratedHours) {
            $iteratedHours->push($hour);
        });

        $this->assertEquals(1, $iteratedHours->count());
    }

    public function testSubHourCustomSubtraction()
    {
        $hour = new Hour('2017-10-10 00:10:10');
        $prevHour = new Hour('2017-10-09 22:00:00');

        $this->assertEquals($prevHour, $hour->prev(2));
    }

    public function testSubHourDefaultSubtraction()
    {
        $hour = new Hour('2017-10-10 00:10:10');
        $prevHour = new Hour('2017-10-09 23:00:00');

        $this->assertEquals($prevHour, $hour->prev());
    }

    public function testToString()
    {
        $hour = new Hour('2017-10-10 00:10:10');

        $this->assertEquals('2017-10-10 00:00:00', (string)$hour);
    }
}
