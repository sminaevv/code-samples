<?php

namespace App\Helpers;

use Carbon\Carbon;

class Hour
{
    /**
     * @var \Carbon\Carbon Конец часового интервала. По факту - нулевая секунда следующего часа,
     * т.к. предназначено для условий сравнения, не включающих это значение во временной диапазон.
     */
    public $end;
    /** @var \Carbon\Carbon */
    public $start;


    /**
     * Iterate through hour period and pass every hour to callback. If callback returns false, loop will be broken.
     * Forward and backword loop directions are allowed.
     *
     * @param \App\Helpers\Hour $from
     * @param \App\Helpers\Hour $to
     * @param callable $callback
     */
    public static function loop(self $from, self $to, callable $callback)
    {
        $next = $from->lessThan($to) ? 'next' : 'prev';

        $cursor = $from;
        do {
            if ($callback($cursor) === false) {
                break;
            }

            if ($cursor->equalTo($to)) {
                break;
            }

            $cursor = $cursor->$next();
        } while (true);
    }

    public function __construct($time = null)
    {
        $t = $time instanceof Carbon ? $time : new Carbon($time);
        $this->start = $t->copy()->minute(0)->second(0);
        $this->end = $this->start->copy()->addHour();
    }

    public function __toString(): string
    {
        return (string)$this->start;
    }

    public function diff(self $hour)
    {
        return $this->start->diffInHours($hour->end);
    }

    public function equalTo(self $hour)
    {
        return $this->start->equalTo($hour->start);
    }

    public function greaterThan(self $hour)
    {
        return $this->start->greaterThan($hour->start);
    }

    public function isCurrent()
    {
        return $this->start->isSameAs('YmdH');
    }

    public function lessThan(self $hour)
    {
        return $this->start->lessThan($hour->start);
    }

    public function next($value = 1): self
    {
        return new self($this->start->copy()->addHour($value));
    }

    public function prev($value = 1): self
    {
        return new self($this->start->copy()->subHour($value));
    }

    /**
     * Determines if the instance is in the past, ie. less (before) than the next hour.
     * The current hour is considered as in the past.
     *
     * @return bool
     */
    public function isPast()
    {
        return $this->lessThan((new Hour())->next());
    }
}
