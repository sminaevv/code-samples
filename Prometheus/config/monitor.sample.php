<?php

return [
    'prometheus' => [
        /*
        |--------------------------------------------------------------------------
        | Metrics list
        |--------------------------------------------------------------------------
        |
        | Metric description array:
        |   - name - must match the regex [a-zA-Z_:][a-zA-Z0-9_:]*
        |   - type - one of counter, gauge, histogram, summary
        |   - labels - array of name-value pairs. Label name must match the regex [a-zA-Z_][a-zA-Z0-9_]*.
        |       Label names beginning with __ are reserved for internal use.
        |       Label values may contain any Unicode characters.
        |   - callable to get metric value
        |
        */
        'metrics' => [
            [
                'name' => 'campaigns_total',
                'type' => 'gauge',
                'labels' => ['state' => 'active'],
                'callback' => ['App\PrometheusMonitor', 'campaignsTotalActive'],
            ],
            [
                'name' => 'queue_size',
                'type' => 'gauge',
                'labels' => ['name' => 'heavy'],
                'callback' => ['App\PrometheusMonitor', 'queueSizeHeavy'],
            ],
        ],
    ],
];
