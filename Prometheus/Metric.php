<?php

namespace Clickadilla\Monitoring\Prometheus;

use InvalidArgumentException;

final class Metric
{
    private $labels;
    private $name;
    private $value;


    public function __construct(string $name, $value, $labels = [])
    {
        $this->name = $this->validatedName($name);
        $this->value = $this->validatedValue($value);
        $this->labels = $labels;
    }

    private function validatedName($value): string
    {
        if (!preg_match('/[a-zA-Z_:][a-zA-Z0-9_:]*/', $value)) {
            throw new InvalidArgumentException('Invalid metric name: ' . $value);
        }

        return $value;
    }

    private function validatedValue($value): float
    {
        if (!is_numeric($value)) {
            throw new InvalidArgumentException('Invalid metric value: ' . $value);
        }

        return (float)$value;
    }


    public function getLabels(): array
    {
        return $this->labels;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getValue(): float
    {
        return $this->value;
    }
}
