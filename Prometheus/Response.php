<?php

namespace Clickadilla\Monitoring\Prometheus;

use Illuminate\Http\Response as BaseResponse;

final class Response extends BaseResponse
{
    private const HEADERS = [
        'Content-Type' => 'text/plain',
    ];
    private const STATUS = 200;


    public function __construct(MetricCollection $data)
    {
        parent::__construct($data, static::STATUS, static::HEADERS);
    }

    public function setContent($content): self
    {
        parent::setContent((new Formatter($content))->asText());

        return $this;
    }
}
