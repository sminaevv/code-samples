<?php

namespace Clickadilla\Monitoring\Prometheus;

use Illuminate\Support\Facades\Response as ResponseFacade;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;

final class ServiceProvider extends IlluminateServiceProvider
{
    public function boot()
    {
        ResponseFacade::macro('forPrometheus', function (MetricCollection $metrics) {
            return new Response($metrics);
        });

        Route::get('/monitor/{format}', Controller::class)
            ->where('format', 'prometheus')
            ->name('monitor.prometheus');
    }
}
