<?php

namespace Clickadilla\Monitoring\Prometheus;

final class Monitor
{
    public function getMetrics(): MetricCollection
    {
        $metrics = new MetricCollection();

        foreach (config('monitor.prometheus.metrics') as $metricParams) {
            $metrics->push(new Metric(
                $metricParams['name'],
                $metricParams['callback'](),
                $metricParams['labels'] ?? []
            ));
        }

        return $metrics;
    }
}
