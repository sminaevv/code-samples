<?php

namespace Clickadilla\Monitoring\Prometheus;

use Illuminate\Routing\Controller as BaseController;

final class Controller extends BaseController
{
    public function __invoke(Monitor $monitor)
    {
        return response()->forPrometheus($monitor->getMetrics());
    }
}
