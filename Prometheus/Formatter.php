<?php

namespace Clickadilla\Monitoring\Prometheus;

final class Formatter
{
    /**
     * @var \Clickadilla\Monitoring\Prometheus\MetricCollection
     */
    private $metrics;


    public function __construct(MetricCollection $metrics)
    {
        $this->metrics = $metrics;
    }


    private function labelsToString(array $labels): string
    {
        $string = '';

        foreach ($labels as $name => $value) {
            $string .= ($string ? ', ' : '') . $name . '=' . $value;
        }

        return $string ? '{' . $string . '}' : '';
    }

    public function asText(): string
    {
        $text = '';

        $this->metrics = $this->metrics->sortBy(function (Metric $metric) {
            return $metric->getName();
        });

        $this->metrics->each(function (Metric $metric) use (&$text) {
            $text .= sprintf(
                "%s%s %f %d\n",
                $metric->getName(),
                $this->labelsToString($metric->getLabels()),
                $metric->getValue(),
                round(microtime(true) * 1000)
            );
        });

        return $text;
    }
}
